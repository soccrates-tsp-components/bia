from enum import Enum, unique

@unique
class DefenseName(Enum):
    HOST_ISOLATION = "Host isolation"
    TRAFFIC_FILTERING = "Traffic filtering"
    NO_DEFENSE = ""


DEFENSE_INFO = {
    DefenseName.HOST_ISOLATION: "Isolate the host from the network by using Network Segmentation",
    DefenseName.TRAFFIC_FILTERING: "Use network appliances to filter ingress or egress traffic and perform protocol-based filtering",
    DefenseName.NO_DEFENSE: "No defense provided"
}


MITRE_REF = {
    DefenseName.HOST_ISOLATION: "M1030",
    DefenseName.TRAFFIC_FILTERING: "M1037",
    DefenseName.NO_DEFENSE: ""
}


class ContainmentDefense():
    def __init__(self, defenseName_str):
        self.setDefenseName(defenseName_str)
        self.defenseInfo = DEFENSE_INFO[self.defenseName]
        self.mitreRef = MITRE_REF[self.defenseName]

    def setDefenseName(self, defenseName_str):
        try:
            self.defenseName = DefenseName(defenseName_str)
        except ValueError as e:
            print(e)
            self.defenseName = DefenseName.NO_DEFENSE

    def isHostIsolation(self):
        return self.defenseName == DefenseName.HOST_ISOLATION

    def isTrafficFiltering(self):
        return self.defenseName == DefenseName.TRAFFIC_FILTERING
