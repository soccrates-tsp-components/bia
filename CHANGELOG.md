## [0.8.11](https://ci.tno.nl/gitlab/soccrates/software/bia/compare/0.8.10...0.8.11) (2022-08-24)


### Bug Fixes

* NameError: name 'StaticFiles' is not defined ([14685e3](https://ci.tno.nl/gitlab/soccrates/software/bia/commit/14685e3c6eb818dbc5b63d305c937b36d5558e1a))

## [0.8.10](https://ci.tno.nl/gitlab/soccrates/software/bia/compare/0.8.9...0.8.10) (2022-08-18)


### Bug Fixes

* ingressRoute ([01fbc55](https://ci.tno.nl/gitlab/soccrates/software/bia/commit/01fbc556f7d9729f948ea0dd60c7133904af82f1))

## [0.8.9](https://ci.tno.nl/gitlab/soccrates/software/bia/compare/0.8.8...0.8.9) (2022-08-16)


### Bug Fixes

* bia model volume ([0642b91](https://ci.tno.nl/gitlab/soccrates/software/bia/commit/0642b912d20ee87fe29cd393d362dd5606f13473))

## [0.8.8](https://ci.tno.nl/gitlab/soccrates/software/bia/compare/0.8.7...0.8.8) (2022-08-15)


### Bug Fixes

* ingressRoutes, regcred, values ([bcedee2](https://ci.tno.nl/gitlab/soccrates/software/bia/commit/bcedee2ca2cb5741970d4ab65fb399c6e194381a))

## [0.8.7](https://ci.tno.nl/gitlab/soccrates/software/bia/compare/0.8.6...0.8.7) (2022-08-11)


### Bug Fixes

* **ci:** moves templates ([6bc1532](https://ci.tno.nl/gitlab/soccrates/software/bia/commit/6bc1532368a3e5d89de3f7465a8d16014b8830ef))

## [0.8.6](https://ci.tno.nl/gitlab/soccrates/software/bia/compare/0.8.5...0.8.6) (2022-08-11)


### Bug Fixes

* helm releases ([73c35a1](https://ci.tno.nl/gitlab/soccrates/software/bia/commit/73c35a1feda660291607db915b794f02b36a1a1f))

## [0.8.5](https://ci.tno.nl/gitlab/soccrates/software/bia/compare/0.8.4...0.8.5) (2022-08-11)


### Bug Fixes

* ci pipeline ([c96fa2f](https://ci.tno.nl/gitlab/soccrates/software/bia/commit/c96fa2f7ef760e79a3da312a64a2d05760dd2d4e))

## [0.8.4](https://ci.tno.nl/gitlab/soccrates/software/bia/compare/0.8.3...0.8.4) (2022-08-11)


### Bug Fixes

* tags ([0c43d5c](https://ci.tno.nl/gitlab/soccrates/software/bia/commit/0c43d5c7c83c8b08682296bad23a41209a1c87f6))

## [0.8.3](https://ci.tno.nl/gitlab/soccrates/software/bia/compare/0.8.2...0.8.3) (2022-08-11)


### Bug Fixes

* regex ([cfd5732](https://ci.tno.nl/gitlab/soccrates/software/bia/commit/cfd5732b154f159381150dad00ed538dee90cdee))
* tags ([05dfb4f](https://ci.tno.nl/gitlab/soccrates/software/bia/commit/05dfb4fa27b210ffe38008e7b8c514cb856c4dab))

## [0.8.3-beta.1](https://ci.tno.nl/gitlab/soccrates/software/bia/compare/0.8.2...0.8.3-beta.1) (2022-08-11)


### Bug Fixes

* regex ([cfd5732](https://ci.tno.nl/gitlab/soccrates/software/bia/commit/cfd5732b154f159381150dad00ed538dee90cdee))
* tags ([05dfb4f](https://ci.tno.nl/gitlab/soccrates/software/bia/commit/05dfb4fa27b210ffe38008e7b8c514cb856c4dab))

# [0.8.0](https://ci.tno.nl/gitlab/soccrates/software/bia/compare/v0.7.6...v0.8.0) (2022-08-11)


### Bug Fixes

* adds authelia enabled true to default values.yaml ([67a490b](https://ci.tno.nl/gitlab/soccrates/software/bia/commit/67a490ba4945a80d799270d2783059c3fbbd932b))
* adds helmignore ([d016a33](https://ci.tno.nl/gitlab/soccrates/software/bia/commit/d016a33c1fc4f2fb87de0fed682e30b1a61b6335))
* closed environment issues ([312e1d0](https://ci.tno.nl/gitlab/soccrates/software/bia/commit/312e1d04b343f112eddae6d19ef37fbde70ea098))
* release helm chart on tags ([a2548f8](https://ci.tno.nl/gitlab/soccrates/software/bia/commit/a2548f81abf6b707816a53f7d8269b8944d5bacc))
* tags ([05dfb4f](https://ci.tno.nl/gitlab/soccrates/software/bia/commit/05dfb4fa27b210ffe38008e7b8c514cb856c4dab))


### Features

* adds helm chart ([228ee9d](https://ci.tno.nl/gitlab/soccrates/software/bia/commit/228ee9da8d014b949901cbf7679775117fb6781b))
