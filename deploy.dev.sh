#!/bin/bash

tag=${DOCKER_TAG:-v0.3.16}
printf "tag is %s\n" "$tag"

export HOST_IP_OR_DOMAIN=localhost
export DEPLOYMENT_UID=$RANDOM

docker build -t bia bia/
docker image tag bia localhost:5000/bia
docker push localhost:5000/bia

docker build -t bia-backend bia-backend/
docker image tag bia-backend localhost:5000/bia-backend
docker push localhost:5000/bia-backend

docker build -t bia-processing bia-processing/
docker image tag bia-processing localhost:5000/bia-processing
docker push localhost:5000/bia-processing

env TAG=${tag} docker stack deploy --prune \
    -c docker-compose.yml -c docker-compose.dev.yml bia
