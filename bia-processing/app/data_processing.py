import datetime
import json
import logging
import os

import act.api
import act.api.fact
import numpy as np
import pandas as pd
from decouple import config
from elastic_netflow import ElasticIngestor

FILES_PATH = "/model/"
FILE_PROBA_AMAT = FILES_PATH + "AdjacencyMatrix_Prob.csv"
FILE_TIME_AMAT = FILES_PATH + "AdjacencyMatrix_time.csv"
FILE_ASSET_INFO = FILES_PATH + "assets_info.json"
FILE_BF_Ano_PROBABILITY = FILES_PATH + "BF_Prob.csv"
FILE_BF_Ano_TIME = FILES_PATH + "BF_time.csv"
FILE_BF_PROBABILITY = FILES_PATH + "BFs_Prob.csv"
FILE_BF_TIME = FILES_PATH + "BFs_time.csv"
FILE_TRAFFIC_MATRIX_PROB = FILES_PATH + "TrafficMatrixProb.csv"
FILE_ASSET_MATRIX_PROB = FILES_PATH + "AssetsMatrixProb.csv"
FILE_ASSET_MATRIX_TIME = FILES_PATH + "AssetsMatrixtime.csv"

act_url = os.environ.get("ACT_URL") or config("ACT_URL")
act_user_id = os.environ.get("ACT_USER_ID") or config("ACT_USER_ID")
act_log_level = os.environ.get("ACT_LOG_LEVEL") or config("ACT_LOG_LEVEL")
act_username = os.environ.get("ACT_USERNAME") or config("ACT_USERNAME")
act_password = os.environ.get("ACT_PASSWORD") or config("ACT_PASSWORD")
act_origin = os.environ.get("ACT_ORIGIN") or config("ACT_ORIGIN")
act_origin_desc = os.environ.get("ACT_ORIGIN_DESCRIPTION") or config(
    "ACT_ORIGIN_DESCRIPTION"
)
logging.basicConfig(
    format="%(levelname)s:%(message)s", level=os.environ.get("LOGGING_LEVEL", "info")
)


def connect_act(
    act_username, act_password, act_url, act_user_id, act_log_level, act_origin
):
    try:
        # http authentication
        if act_username and act_password:
            return act.api.Act(
                act_url,
                user_id=act_user_id,
                log_level=act_log_level,
                origin_name=act_origin,
                requests_common_kwargs={"auth": (act_username, act_password)},
            )
        else:
            # anonymous connection
            return act.api.Act(
                act_url,
                user_id=act_user_id,
                log_level=act_log_level,
            )
    except Exception as error:
        logging.critical(f"Unable to connect to {act_url} with error {error}")


def getNetflowsFromElastic():
    elastic = ElasticIngestor()

    logging.debug("Start importing netflow from Elasticsearch")

    count = 0

    # fetch from elastic
    for flow in elastic.fetch_flows():
        yield flow

        if count % 100 == 0:
            logging.info(f"Fetched {count} flows. Last={flow}")

        count += 1


def getHostIPv4FromIMC(act_conn):
    IPv4_list = []
    UUIDs_list = []
    hosts_list = []

    # for assignedTo facts source is always `ipv4` and destination_object is `host`
    for fact in act_conn.fact_search(fact_type="assignedTo", limit=100000):
        IPv4_list.append(fact.source_object.value)
        hosts_list.append(fact.destination_object.value)
        UUIDs_list.append(fact.destination_object.id)

    return IPv4_list, UUIDs_list, hosts_list


def buildModel():
    # initiate act connection
    logging.debug(f"connecting to act at {act_url}")
    act_conn = connect_act(
        act_username, act_password, act_url, act_user_id, act_log_level, act_origin
    )
    logging.debug(f"connected to act at {act_url}")

    # get ipv4/UUIDs and hosts
    IPv4_list, UUIDs_list, hosts_list = getHostIPv4FromIMC(act_conn)

    FILE_ASSET_INFO = createAssetsInfo(hosts_list, UUIDs_list, IPv4_list)
    with open(FILE_ASSET_INFO, "r") as model:
        Assets_info = json.load(model)

    FILE_TRAFFIC_MATRIX_PROB = createTrafficMatrix(act_conn, Assets_info, IPv4_list)
    FILE_ASSET_MATRIX_PROB = createAssetsMatrix()
    FILE_BF_PROBABILITY = processBFsProbMatrix(Assets_info)
    FILE_BF_TIME = processBFsTimeMatrix(Assets_info)
    FILE_ASSET_MATRIX_TIME = createAssetsTimeMatrix()
    (
        FILE_ASSET_INFO,
        FILE_ASSET_MATRIX_PROB,
        FILE_ASSET_MATRIX_TIME,
    ) = removeDisconnectedAssets(
        Assets_info, FILE_ASSET_MATRIX_PROB, FILE_ASSET_MATRIX_TIME, FILE_BF_PROBABILITY
    )
    FILE_PROBA_AMAT = createAdjProbMatrix()
    FILE_TIME_AMAT = createAdjTimeMatrix()
    return (
        FILE_ASSET_INFO,
        FILE_ASSET_MATRIX_PROB,
        FILE_BF_PROBABILITY,
        FILE_BF_TIME,
        FILE_PROBA_AMAT,
        FILE_ASSET_MATRIX_TIME,
        FILE_TIME_AMAT,
    )


def createAssetsInfo(hosts_list, UUIDs_list, IPv4_list):
    Assets_Info = {"nodes": []}
    for i in range(len(hosts_list)):
        asset = {}
        asset["id"] = i
        asset["name"] = "asset" + str(i)
        asset["UUID"] = UUIDs_list[i]
        asset["host_name"] = hosts_list[i]
        asset["IP"] = IPv4_list[i]
        Assets_Info["nodes"].append(asset)
    with open(FILE_ASSET_INFO, "w") as Infos_file:
        json.dump(Assets_Info, Infos_file)
    return FILE_ASSET_INFO


def createTrafficMatrix(act_conn, Assets_info, IPv4_list):
    Assets_name = []
    for i in range(len(Assets_info["nodes"])):
        Assets_name.append(Assets_info["nodes"][i]["name"])

    Traffic_Matrix = pd.DataFrame(columns=Assets_name, index=Assets_name)
    Traffic_Matrix.fillna(0, inplace=True)

    netflow_Flows = []
    for netflow in getNetflowsFromElastic():
        netflow_Flows.append(
            [
                netflow["src_ip"],
                netflow["dst_ip"],
                netflow["bytes_in"],
            ]
        )
        netflow_Flows.append(
            [
                netflow["dst_ip"],
                netflow["src_ip"],
                netflow["bytes_out"],
            ]
        )

    # Delete IP addresses that do not exist in the model
    netflow_To_Mat = []
    for i in range(len(netflow_Flows)):
        if (netflow_Flows[i][1] in IPv4_list) and (netflow_Flows[i][0] in IPv4_list):
            netflow_To_Mat.append(netflow_Flows[i])

    for i in range(len(netflow_To_Mat)):
        Traffic_Matrix[Get_name_from_ip(netflow_To_Mat[i][1], Assets_info)][
            Get_name_from_ip(netflow_To_Mat[i][0], Assets_info)
        ] += int(netflow_To_Mat[i][2])

    Traffic_Matrix.index.name = "node"
    Traffic_Matrix.to_csv(FILE_TRAFFIC_MATRIX_PROB)
    return FILE_TRAFFIC_MATRIX_PROB


def createAssetsMatrix():
    TrafficMatrix = pd.read_csv(FILE_TRAFFIC_MATRIX_PROB, index_col=0)
    TrafficMatrix = TrafficMatrix.astype(float)
    column_sum = np.sum(TrafficMatrix.values, axis=0)

    for i in range(len(TrafficMatrix.values)):
        for j in range(len(TrafficMatrix.values[i])):
            if column_sum[j] != 0:
                TrafficMatrix.values[i][j] = round(
                    TrafficMatrix.values[i][j] / column_sum[j], 2
                )
            else:
                TrafficMatrix.values[i][j] = 0

    TrafficMatrix.to_csv(FILE_ASSET_MATRIX_PROB)
    return FILE_ASSET_MATRIX_PROB


def processBFsProbMatrix(Assets_info):
    BFsProb = pd.read_csv(FILE_BF_Ano_PROBABILITY, index_col=0)
    BFs_Prob_columns = list(BFsProb.columns)
    BFs_Prob_rows = list(BFsProb.index)
    for i in range(len(BFs_Prob_rows) - len(BFs_Prob_columns)):
        BFs_Prob_rows[i] = Get_name_from_uuid(BFs_Prob_rows[i], Assets_info)
    BFsProb = pd.DataFrame(
        BFsProb.values, columns=BFs_Prob_columns, index=BFs_Prob_rows
    )
    BFsProb.index.name = "node"
    BFsProb.to_csv(FILE_BF_PROBABILITY)
    return FILE_BF_PROBABILITY


def processBFsTimeMatrix(Assets_info):
    BFstime = pd.read_csv(FILE_BF_Ano_TIME, index_col=0)
    BFs_time_columns = list(BFstime.columns)
    BFs_time_rows = list(BFstime.index)
    for i in range(len(BFs_time_rows) - len(BFs_time_columns)):
        BFs_time_rows[i] = Get_name_from_uuid(BFs_time_rows[i], Assets_info)
    BFstime = pd.DataFrame(
        BFstime.values, columns=BFs_time_columns, index=BFs_time_rows
    )
    BFstime.index.name = "node"
    BFstime.to_csv(FILE_BF_TIME)
    return FILE_BF_TIME


def createAdjProbMatrix():
    AssetsMatrix_Prob = pd.read_csv(FILE_ASSET_MATRIX_PROB, index_col=0)
    BFsMatrix_Prob = pd.read_csv(FILE_BF_PROBABILITY)
    AdjacencyMatrix_Prob = AssetsMatrix_Prob.merge(
        BFsMatrix_Prob, on="node", how="outer"
    )
    AdjacencyMatrix_Prob.fillna(0, inplace=True)
    AdjacencyMatrix_Prob.to_csv(FILE_PROBA_AMAT, index=False)
    return FILE_PROBA_AMAT


def createAssetsTimeMatrix():
    BFsMatrix_Prob = pd.read_csv(FILE_BF_PROBABILITY)
    BFsMatrix_time = pd.read_csv(FILE_BF_TIME, index_col=0)
    BFsMatrix_time_values = []
    for i in range(len(BFsMatrix_Prob.values)):
        BFsMatrix_time_values += list(
            filter((0).__ne__, [int(val) for val in BFsMatrix_time.values[i]])
        )
    min_value = 0.1 * min(BFsMatrix_time_values)
    AssetsMatrix_time = pd.read_csv(FILE_ASSET_MATRIX_PROB, index_col=0)
    AssetsMatrix_time_columns = list(AssetsMatrix_time.columns)
    AssetsMatrix_time_rows = list(AssetsMatrix_time.index)
    for i in range(len(AssetsMatrix_time.values)):
        for index, value in enumerate(AssetsMatrix_time.values[i]):
            if value != 0.0:
                if min_value >= 0.1:
                    AssetsMatrix_time.values[i][index] = min_value
                else:
                    AssetsMatrix_time.values[i][index] = 0.1
    AssetsMatrix_time = pd.DataFrame(
        AssetsMatrix_time.values,
        columns=AssetsMatrix_time_columns,
        index=AssetsMatrix_time_rows,
    )
    AssetsMatrix_time.index.name = "node"
    AssetsMatrix_time.to_csv(FILE_ASSET_MATRIX_TIME)
    return FILE_ASSET_MATRIX_TIME


def createAdjTimeMatrix():
    AssetsMatrix_time = pd.read_csv(FILE_ASSET_MATRIX_TIME, index_col=0)
    BFsMatrix_time = pd.read_csv(FILE_BF_TIME)
    AdjacencyMatrix_time = AssetsMatrix_time.merge(
        BFsMatrix_time, on="node", how="outer"
    )
    AdjacencyMatrix_time.fillna(0, inplace=True)
    AdjacencyMatrix_time.to_csv(FILE_TIME_AMAT, index=False)
    return FILE_TIME_AMAT


def Get_name_from_uuid(host_UUID: str, Assets_info):
    for i in range(len(Assets_info["nodes"])):
        if Assets_info["nodes"][i]["UUID"] == host_UUID:
            return Assets_info["nodes"][i]["name"]


def Get_name_from_ip(ip: str, Assets_info):
    for i in range(len(Assets_info["nodes"])):
        if Assets_info["nodes"][i]["IP"] == ip:
            return Assets_info["nodes"][i]["name"]


def removeDisconnectedAssets(
    Assets_info, FILE_ASSET_MATRIX_PROB, FILE_ASSET_MATRIX_TIME, FILE_BF_PROBABILITY
):
    AssetsMatrix_Prob = pd.read_csv(FILE_ASSET_MATRIX_PROB, index_col=0)
    AssetsMatrix_time = pd.read_csv(FILE_ASSET_MATRIX_TIME, index_col=0)
    nameNodes = AssetsMatrix_Prob.columns.values.tolist()
    # find the disconnected assets
    ##before checking the link with the business functions
    Disconnected_Assets = []
    for i in nameNodes:
        if all(AssetsMatrix_Prob[i][j] == 0 for j in nameNodes) and all(
            AssetsMatrix_Prob[j][i] == 0 for j in nameNodes
        ):
            Disconnected_Assets.append(i)
    ##save the nodes that support the business functions
    BFsMatrix_Prob = pd.read_csv(FILE_BF_PROBABILITY, index_col=0)
    number_of_BFs = len(BFsMatrix_Prob.columns)
    number_of_AssetsSupportedBFs_BFs = len(BFsMatrix_Prob)
    name_AssetsSupportedBFs_BFs = list(BFsMatrix_Prob.index)
    Assets_Supported_BFs = name_AssetsSupportedBFs_BFs[
        0 : number_of_AssetsSupportedBFs_BFs - number_of_BFs
    ]
    for i in Assets_Supported_BFs:
        if i in Disconnected_Assets:
            Disconnected_Assets.remove(i)

    # remove disconnected assets (AssetsMatrix_Prob)
    AssetsMatrix_Prob.drop(Disconnected_Assets, inplace=True)
    AssetsMatrix_Prob.drop(columns=Disconnected_Assets, inplace=True)
    AssetsMatrix_Prob.to_csv(FILE_ASSET_MATRIX_PROB)

    # remove disconnected assets (AdjacencyMatrix_time)
    AssetsMatrix_time.drop(Disconnected_Assets, inplace=True)
    AssetsMatrix_time.drop(columns=Disconnected_Assets, inplace=True)
    AssetsMatrix_time.to_csv(FILE_ASSET_MATRIX_TIME)

    # remove disconnected assets (Assets_info file)
    Assets_to_be_removed = []
    for i in range(len(Assets_info["nodes"])):
        for j in Disconnected_Assets:
            if Assets_info["nodes"][i]["name"] == j:
                Assets_to_be_removed.append(Assets_info["nodes"][i])

    for asset in Assets_to_be_removed:
        Assets_info["nodes"].remove(asset)

    # update asset indices
    for i in range(len(Assets_info["nodes"])):
        Assets_info["nodes"][i]["id"] = i
    with open(FILE_ASSET_INFO, "w") as Infos_file:
        json.dump(Assets_info, Infos_file)

    return FILE_ASSET_INFO, FILE_ASSET_MATRIX_PROB, FILE_ASSET_MATRIX_TIME


def toJSON(self):
    return json.dumps(self, default=lambda o: o.__dict__, sort_keys=True, indent=4)
