network:
	docker network create --driver overlay soccrates || true
	docker network create --driver overlay proxy || true

bia: network
	echo bia will be at https://bia.$$HOST_IP_OR_DOMAIN
	bash deploy.sh
